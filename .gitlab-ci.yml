# Maven Template abgespeckt

variables:
  # This will suppress any download for dependencies and plugins or upload messages which would clutter the console log.
  # `showDateTime` will show the passed time in milliseconds. You need to specify `--batch-mode` to make this work.
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  # As of Maven 3.3.0 instead of this you may define these options in `.mvn/maven.config` so the same config is used
  # when running from the command line.
  # `installAtEnd` and `deployAtEnd` are only effective with recent version of the corresponding plugins.
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"

# This template uses jdk8 for verifying and deploying images
image: maven:3-alpine

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
cache:
  paths:
    - .m2/repository

stages:
    - build
    - release
    - test
    - visualize
    - deploy
    - cleanup

build_job:
    stage: build
    before_script:
      - apk update
      - apk add gettext
    script:
        - mvn clean compile assembly:single
        - echo $CI_JOB_ID >target/job.pid        
        - 'envsubst <deployment.yaml >target/deployment.yaml'
        - 'envsubst <service.yaml >target/service.yaml'
    artifacts:
        paths:
            - target

# Release als Download 
release_artifacts_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo 'running release_job'
  needs:
    - job: build_job
      artifacts: true  
  rules:
  - if: "$CI_COMMIT_TAG"
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli'
    # tag_name is a mendatory field and can not be an empty string
    tag_name: '$CI_COMMIT_TAG'
    assets:
      links:
        - name: 'Build Artifacts'
          url: '${CI_PROJECT_URL}/-/jobs/$(cat target/job.pid)/artifacts/download?file_type=archive'

# Release als Container Image mit Tag
release_job:
  stage: release
  # Use the official docker image.
  image: docker:latest
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --pull -t "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG" .
    - docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG"
  artifacts:
      paths:
          - target    
  needs:
    - job: build_job
      artifacts: true  
  rules:
  - if: "$CI_COMMIT_TAG"

# Deploy to Kubernetes
deploy_job:
  stage: deploy
  tags:
    - kubernetes
  # Container Image mit kubectl
  image: bitnami/kubectl:latest
  script:
    - kubectl apply -n default -f target/deployment.yaml
    - kubectl apply -n default -f target/service.yaml  

  needs:
    - job: release_job
      artifacts: true  
  rules:
  - if: "$CI_COMMIT_TAG"

test_unit_job:
  stage: test
  script:
    - mvn verify
  artifacts:
    when: always
    reports:
      junit:
        - target/surefire-reports/TEST-*.xml
        - target/failsafe-reports/TEST-*.xml

test-jdk11:
  stage: test
  image: maven:3.6.3-jdk-11
  script:
    - mvn $MAVEN_CLI_OPTS clean org.jacoco:jacoco-maven-plugin:prepare-agent test jacoco:report
  artifacts:
    paths:
      - target/site/jacoco/jacoco.xml

coverage-jdk11:
  # Must be in a stage later than test-jdk11's stage.
  # The `visualize` stage does not exist by default.
  # Please define it first, or choose an existing stage like `deploy`.
  stage: visualize
  image: registry.gitlab.com/haynes/jacoco2cobertura:1.0.7
  script:
    # convert report from jacoco to cobertura, using relative project path
    - python /opt/cover2cover.py target/site/jacoco/jacoco.xml $CI_PROJECT_DIR/src/main/java/ > target/site/cobertura.xml
  needs: ["test-jdk11"]
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: target/site/cobertura.xml     

include:
  - template: Code-Quality.gitlab-ci.yml   
  - template: Security/SAST.gitlab-ci.yml
